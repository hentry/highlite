import React from 'react';
import PropTypes from 'prop-types';

const SVGRenderer = (props) => {
	return (
		<span>
			{
				props.show ?
				<props.svg width={props.width} height={props.height} /> :
				undefined
			}
		</span>
	);
};

SVGRenderer.defaultProps = {
	props: {},
	show: true,
};

SVGRenderer.propTypes = {
	props: PropTypes.object,
	show: PropTypes.bool,
};

export default SVGRenderer;
