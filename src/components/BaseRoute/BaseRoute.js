import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LayoutContainer from './../../containers/Layout/LayoutContainer';
import LoginContainer from './../../containers/Login/LoginContainer';
import ForgotPassword from './../../containers/ForgotPassword/ForgotPasswordContainer';
import ResetPassword from './../../containers/ResetPassword/ResetPasswordContainer';
import SignUp from './../../containers/SignUp/SignUp';
import Logout from './../Logout/Logout';
import { Route } from 'react-router-dom';

class BaseRoute extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.location.pathname === '/') this.context.router.history.push('login');

    setTimeout(() => {
      const isExtensionInstalled = document.getElementById('cfhdojbkjhnklbpkdaibdccddilifddb');

      if (!isExtensionInstalled) {
        this.props.showInstallModal();
      }
    }, 2000);
  }

  render() {
    return (
      <div>
        <Route path="/login" component={LoginContainer}/>
        <Route path="/signup" component={SignUp}/>
        <Route path="/logout" component={Logout}/>
        <Route path="/forgotpassword" component={ForgotPassword}/>
        <Route path="/resetPassword/:hash" component={ResetPassword}/>
        <Route path="/hl" component={LayoutContainer}/>
      </div>
    );
  }
}

BaseRoute.contextTypes = {
  router: PropTypes.object,
};

export default BaseRoute;