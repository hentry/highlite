import React, { Component } from 'react';
import './Sidebar.scss';
import SVGRenderer from './../SVGRenderer/SVGRenderer';
import HighLightSidebarIcon from './../../../public/images/icons/highlighter-sidebar-icon.svg';
import HighLightSidebarActiveIcon from './../../../public/images/icons/highlighter-sidebar-active-icon.svg';
import FolderIcon from './../../../public/images/icons/folder.svg';
import CreateIcon from './../../../public/images/icons/add.svg';
import PropType from 'prop-types';
import CreateActiveIcon from './../../../public/images/icons/add-active.svg';
import Input from './../FormComponents/Input/Input';
import { getToken } from './../../utils/storageUtils';
import classnames from 'classnames';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.isActiveState = this.isActiveState.bind(this);
  }

  componentDidMount() {
    const token = getToken();
    console.log(token, 'token');
    if (token === 'undefined' || !token) {
      this.context.router.history.push('/login');
      return;
    }
    this.props.fetchFolders();
  }

  onKeyPress(event) {
    if (event.which === 13) {
      this.props.createFolder(this.props.folderName);
    }
  }

  isActiveState(path) {
    return this.context.router.route.location.pathname === path;
  }

  render() {
    return (
      <div className="sidebar">
        <ul className="sidebar__menu">
          <li
            className={classnames('sidebar__menu__item hover', {'active': this.isActiveState('/hl/highlight')})}
            onMouseEnter={this.props.allHighlightsMouseEnter}
            onMouseLeave={this.props.allHighlightsMouseLeave}
            onClick={() => {this.props.navigateTo('/hl/highlight', this.context);}}>
            {
              this.props.allhighlightsActive ||  this.isActiveState('/hl/highlight') ?
              <SVGRenderer svg={HighLightSidebarActiveIcon} width={15} height={15}/> :
              <SVGRenderer svg={HighLightSidebarIcon} width={15} height={15}/>
            }
            <span className="sidebar__menu__item__text">All Highlights</span>
          </li>
          <li
            className="sidebar__menu__item">
            <SVGRenderer svg={FolderIcon} width={15} height={15}/>
            <span className="sidebar__menu__item__text">My Folders</span>
          </li>
          <ul className="sidebar__menu__folder-list">
            {
              this.props.folders.length === 0 &&
              <li className="sidebar__menu__folder-list__no-folders">No folders available</li>
            }
            {
              this.props.folders.map((folder) => {
                const color = {
                  backgroundColor: folder.colorCode,
                };
                return (
                  <li
                    className={classnames('sidebar__menu__folder-list__folder', {'active': this.isActiveState('/hl/highlight/' + folder.id)})}
                    onClick={() => {this.props.navigateTo('/hl/highlight/' + folder.id, this.context);}}>
                    <span className="sidebar__menu__folder-list__folder__circle" style={color}></span>
                    <span className="sidebar__menu__folder-list__folder__text">{folder.name}</span>
                  </li>
                );
              })
            }
          </ul>

          {
            this.props.showInput &&
            <li className="sidebar__menu__add-folder-wrapper">
              <Input
                className="add-folder"
                type="text"
                placeholder="Enter folder name"
                onChange={this.props.onFolderNameChange}
                value={this.props.folderName}
                onKeyPress={this.onKeyPress}
              />
            </li>
          }

          <li
            className="sidebar__menu__item hover"
            onMouseEnter={this.props.createFolderMouseEnter}
            onMouseLeave={this.props.createFolderMouseLeave}
            onClick={this.props.addFolder}>
              {
                this.props.createFolderActive ?
                <SVGRenderer svg={CreateActiveIcon} width={15} height={15}/> :
                <SVGRenderer svg={CreateIcon} width={15} height={15}/>
              }
              <span className="sidebar__menu__item__text">Create Folder</span>
          </li>
        </ul>
      </div>
    );
  }
}

Sidebar.defaultProps = {
  folders: [],
  showInput: false,
};

Sidebar.propTypes = {
  folder: PropType.array,
  showInput: PropType.bool,
};

Sidebar.contextTypes = {
  router: PropType.object,
};

export default Sidebar;
