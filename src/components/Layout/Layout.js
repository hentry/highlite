import React, {Component} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HighlightsContainer from './../../containers/Highlights/HighlightsContainer';
import Sidebar from './../../containers/Sidebar/SidebarContainer';
import Header from './../../containers/Header/HeaderContainer';
import Settings from './../../containers/Settings/SettingsContainer';
import Upgrade from './../../containers/Upgrade/UpgradeContainer';
import PropTypes from 'prop-types';
import { getToken } from './../../utils/storageUtils';
import Modal from './../Modal/Modal';
import './Layout.scss';

class TwoColumnLayout extends Component {
  constructor(props, match) {
    super(props);
  }

  componentDidMount() {
    const token = getToken();
    console.log(token, 'token');
    if (token === 'undefined' || !token) {
      this.context.router.history.push('login');
      return;
    }
    this.props.fetchData();
  }

  render() {
    return (
      <div className="layout">
        {
          this.props.showModal &&
          <Modal
            installExtension={this.props.installExtension}
            cancelModal={this.props.cancelModal}
          />
        }
        <div className="layout__header-wrapper">
          <Header/>
        </div>
        <div className="layout__content-wrapper">
          <Sidebar/>
          <Route path={`${this.props.match.url}/highlight/:folderId?`} component={HighlightsContainer} />
          <Route path={`${this.props.match.url}/settings`} component={Settings}/>
          <Route path={`${this.props.match.url}/upgrade`} component={Upgrade}/>
        </div>
      </div>
    );
  }
}

TwoColumnLayout.contextTypes = {
  router: PropTypes.object,
};

export default TwoColumnLayout;