import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Logo from './../Logo/Logo';
import Input from './../FormComponents/Input/Input';
import Button from './../FormComponents/Button/Button';
import Notifications from './../Notifications/Notifications';
import ReactGA from 'react-ga';
import Loader from './../Loader/Loader';
import Overlay from './../Overlay/Overlay';
import './SignUp.scss';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.onClickSignUp = this.onClickSignUp.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      ReactGA.pageview('/signup');
    }, 400);
  }

  onClickSignUp() {

    if (!this.props.email || !this.props.password || !this.props.name || !this.props.confirmPassword) return;

    if (this.props.confirmPassword !== this.props.password) {
      this.props.passwordMismatch();
      return;
    }
    const params = {
      email: this.props.email,
      password: this.props.password,
      name: this.props.name,
    };

    ReactGA.event({
      category: 'User',
      action: 'clicked',
      label: 'USER_SIGNED_UP',
    });

    this.props.onClickSignUp(params);
  }

  onKeyPress(e) {
    if (e.which === 13) {
      this.onClickSignUp();
    }
  }

  render() {
    return (
      <div className="signup-wrapper">
        {
          this.props.isLoading &&
          <Overlay className="highlights-page-overlay">
            <Loader />
          </Overlay>
        }
        <div className="form">
          <Logo className="logo"/>
          <Input
            onChange={this.props.onUserNameChange}
            value={this.props.name}
            label="Name"
            placeholder=""
            type="text"
            className="material"
            onKeyPress={this.onKeyPress}
          />
          <Input
            onChange={this.props.onUserEmailChange}
            value={this.props.email}
            label="Email ID"
            placeholder=""
            type="email"
            className="material"
            onKeyPress={this.onKeyPress}
          />
          <Input
            onChange={this.props.onPasswordChange}
            value={this.props.password}
            label="Password"
            placeholder=""
            type="password"
            className="material"
            onKeyPress={this.onKeyPress}
          />
          <Input
            onChange={this.props.onConfirmPasswordChange}
            value={this.props.confirmPassword}
            label="Confirm Password"
            placeholder=""
            type="password"
            className="material"
            onKeyPress={this.onKeyPress}
            errorMessage="Your password and confirmation password do not match"
            showError={this.props.isPasswordMismatchError}
          />
          <Button
            value="Sign Up"
            className="signup"
            onClick={this.onClickSignUp}
          />
          <div className="login-label">
            Already have an account? <Link to="/login">Log In</Link> here
          </div>
        </div>
        <Notifications
          show={this.props.isSignupSuccess}
          message="User Signed up Successfully"
        />
      </div>
    );
  }
}

export default connect()(SignUp);
