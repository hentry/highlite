import React from 'react';
import FAV from './../../../public/images/icons/favourite.svg';
import './ToggleIcons.scss';

const FavouriteToggleIcon = () => {
  return (
    <div className="favourite-toggle-icon">
      <FAV width={15} height={15}/>
    </div>
  );
};

export default FavouriteToggleIcon;
