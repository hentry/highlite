import React from 'react';
import classnames from 'classnames';
import './Overlay.scss';

const Overlay = (props) => {
  return (
    <div className={classnames('overlay', props.className)}>
      {props.children}
    </div>
  );
};

export default Overlay;
