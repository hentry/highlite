import React, { Component } from 'react';
import Button from './../FormComponents/Button/Button';
import Input from './../FormComponents/Input/Input';
import Loader from './../Loader/Loader';
import Overlay from './../Overlay/Overlay';
import { getUser } from './../../utils/storageUtils';
import ReactGA from 'react-ga';
import './Settings.scss';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.onSaveProfileInfo = this.onSaveProfileInfo.bind(this);
    this.changePassord = this.changePassord.bind(this);
  }

  componentDidMount() {
    const user = getUser();
    this.props.fetchUser(user);
    setTimeout(() => {
      ReactGA.pageview('/hl/settings');
    }, 400);
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
  }

  onSaveProfileInfo() {
    const data = {
      name: this.props.name,
      email: this.props.email,
    };

    console.log(data);
    this.props.onSaveProfileInfo(data);
  }

  changePassord() {
    const data = {
      password: this.props.password,
      newpassword: this.props.newpassword,
    };

    this.props.changePassord(data);
  }

  render() {
    return (
      <div className="settings">
        {
          this.props.isLoading &&
          <Overlay className="highlights-page-overlay">
            <Loader />
          </Overlay>
        }
        <div className="settings__title">
          Profile
        </div>
        <div className="settings__form">
          <div className="settings__form__row">
            <Input
              className="material"
              label="Name"
              value={this.props.name}
              placeholder=""
              onChange={(value) => {this.props.onChange(value.target.value, 'name');}}
            />
          </div>
          <div className="settings__form__row">
            <Input
              className="material"
              label="Email"
              value={this.props.email}
              placeholder=""
              onChange={(value) => {this.props.onChange(value.target.value, 'email');}}
            />
          </div>
          <div className="settings__form__row">
            <Button
              value="Save Changes"
              onClick={this.onSaveProfileInfo}
              type="submit"
              className="update"
            />
          </div>
        </div>
        <div className="settings__title">
          Change Password
        </div>
        <div className="settings__form">
          <div className="settings__form__row">
            <Input
              className="material"
              label="Password"
              type="password"
              value={this.props.password}
              placeholder=""
              onChange={(value) => {this.props.onChange(value.target.value, 'password');}}
            />
          </div>
          <div className="settings__form__row">
            <Input
              className="material"
              label="New Password"
              type="password"
              value={this.props.newpassword}
              placeholder=""
              onChange={(value) => {this.props.onChange(value.target.value, 'newpassword');}}
            />
          </div>
          <div className="settings__form__row">
            <Button
              value="Save Changes"
              onClick={this.changePassord}
              type="submit"
              className="update"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Settings;
