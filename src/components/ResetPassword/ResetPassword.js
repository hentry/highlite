import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Input from './../FormComponents/Input/Input';
import Button from './../FormComponents/Button/Button';
import Logo from './../Logo/Logo';
import Notifications from './../Notifications/Notifications';
import './ResetPassword.scss';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.hash = decodeURIComponent(this.props.match.params.hash);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const data = {
      newpassword: this.props.newpassword,
      hash: this.hash,
    };

    this.props.resetPassword(data);
  }

  render() {
    return (
      <div className="reset-password">
        <div className="form">
          <Logo className="logo"/>
          <Input
            onChange={(input) => {this.props.onChange(input.target.value, 'newpassword');}}
            value={this.props.newpassword}
            placeholder=""
            type="password"
            label="New Password"
            className="material"
          />
          <Button value="Change Password" className="reset-password-btn" onClick={this.onClick}/>
          <div className="signup-label">
            Go back to Login? <Link to="/login">Log In</Link> here
          </div>
        </div>
        <Notifications
          show={this.props.isResetted}
          message="Password successfully changed"
        />
      </div>
    );
  }
}

export default ResetPassword;
