import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Logo from './../Logo/Logo';
import Input from './../FormComponents/Input/Input';
import Button from './../FormComponents/Button/Button';
import PropTypes from 'prop-types';
import { getToken } from './../../utils/storageUtils';
import Loader from './../Loader/Loader';
import Overlay from './../Overlay/Overlay';
import ReactGA from 'react-ga';
import './Login.scss';

class Login extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentWillMount() {
    const token = getToken();
    if (token === 'undefined' || !token) return;
    this.context.router.history.push('hl/highlight');

    setTimeout(() => {
      ReactGA.pageview('/login');
    }, 400);
  }

  onClick() {
    if (!this.props.email || !this.props.password) return;

    ReactGA.event({
      category: 'User',
      action: 'clicked',
      label: 'USER_LOGGED_IN',
    });
    this.props.doLogin(this.props.email, this.props.password, this.context); 
  }

  onKeyPress(e) {
    if (e.which === 13) {
      this.onClick();
    }
  }

  render() {
    return (
      <div className="login-wrapper">
        {
          this.props.isLoading &&
          <Overlay className="highlights-page-overlay">
            <Loader />
          </Overlay>
        }
        <div className="form">
          <Logo className="logo"/>
          <Input
            onChange={this.props.onUserNameChange}
            value={this.props.email}
            placeholder=""
            type="email"
            label="Email ID"
            className="material"
            errorMessage="This email id is not associated with HighLite. Please create an account."
            showError={this.props.isUserNotAvailable}
            onKeyPress={this.onKeyPress}
          />
          <Input
            onChange={this.props.onPasswordChange}
            value={this.props.password}
            label="Password"
            placeholder=""
            type="password"
            className="material"
            errorMessage="Entered Password is incorrect. Please try again."
            showError={this.props.isAuthFailed}
            onKeyPress={this.onKeyPress}
          />
          <Button value="Log In" className="login" onClick={this.onClick}/>
          <div className="signup-label">
            Don't have an account? <Link to="/signup">Sign Up</Link> here
          </div>
          <div className="forget-password"><Link to="/forgotpassword">Forget Password?</Link></div>
        </div>
      </div>
    );
  }
}

Login.contextTypes = {
  router: PropTypes.object,
};

export default connect()(Login);
