import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Label.scss';

const Label = (props) => {
  return (
    <div className={classnames('label', props.className)}>{props.value}</div>
  );
};

Label.defaultProps = {
  value: 'Value',
};

Label.propTypes = {
  className: PropTypes.string,
  value: PropTypes.string,
};

export default Label;