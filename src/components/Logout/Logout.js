import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from './../Loader/Loader';
import { removeItem } from './../../utils/storageUtils';
import ReactGA from 'react-ga';
import './Logout.scss';

class Logout extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    removeItem('token');
    removeItem('user');
    setTimeout(() => {
      ReactGA.pageview('/logout');
    }, 400);
    setTimeout(() => {
      this.context.router.history.push('login');
    }, 800);
  }

  render() {
    return (
      <div className="logout">
        <Loader />
        <div className="logout__text">Logging out</div>
      </div>
    );
  }
}

Logout.contextTypes = {
  router: PropTypes.object
};


export default Logout;
