import React from 'react';
import Halogen from 'halogen';
import PropTypes from 'prop-types';
import './Loader.scss';

const Loader = (props) => {
  const getLoaderByType = () => {
    return <Halogen.ClipLoader color={props.color} />;
  };

  return (
    <div className="loader">
      {getLoaderByType()}
    </div>
  );
};

Loader.defaultProps = {
  color: '#ECAA27',
  type: 'BounceLoader',
};

Loader.propTypes = {
  color: PropTypes.string,
  type: PropTypes.string,
};

export default Loader;
