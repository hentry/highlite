import React, { Component } from 'react';
import Input from './../FormComponents/Input/Input';
import Button from './../FormComponents/Button/Button';
import Logo from './../Logo/Logo';
import Notifications from './../Notifications/Notifications';
import './ForgotPassword.scss';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const params = {
      email: this.props.email,
    };
    this.props.resetPassword(params);
  }

  render() {
    return (
      <div className="login-wrapper">
        <div className="form">
          <Logo className="logo"/>
          <div className="text">Enter your email below and We will send you a link to reset the password.</div>
          <Input
            onChange={(input) => {this.props.onChange(input.target.value, 'email');}}
            value={this.props.email}
            placeholder=""
            type="email"
            label="User Email"
            className="material"
          />
          <Button value="Reset Password" className="login" onClick={this.onClick}/>
        </div>
        <Notifications
          show={this.props.isForgotPasswordSuccess}
          message="Password reset link is sent to your email"
        />
      </div>
    );
  }
}

export default ForgotPassword;
