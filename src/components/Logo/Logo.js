import React from 'react';
import PropTypes from 'prop-types';
import Img from 'react-image';
import SVG from './../../../public/images/icons/Logo.svg';
import './Logo.scss';

const Logo = (props) => {
  return (
    <div className="logo">
    	<SVG width={props.width} height={props.height} className={props.className}/>
    </div>
  );
};

Logo.defaultProps = {
  className: '',
};

Logo.propTypes = {
  className: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

export default Logo;
