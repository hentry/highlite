import React from 'react';
import ModalComp from 'react-modal';
import Button from './../FormComponents/Button/Button';
import './Modal.scss';

const Modal = (props) => {
  return (
    <div className="highlite-modal">
      <ModalComp
        isOpen
        contentLabel="Modal"
      >
      <div className="highlite-modal__text">Seems like you didnt install the Highlite Extension. Do you wanna install it?</div>
      <div className="highlite-modal__button-wrapper">
        <div className="highlite-modal__button-wrapper__column">
          <Button
            value="Install"
            onClick={props.installExtension}
          />
        </div>
        <div className="highlite-modal__button-wrapper__column">
          <Button
            value="Cancel"
            onClick={props.cancelModal}
            className="disabled"
          />
        </div>
      </div>
      </ModalComp>
    </div>
  );
};

export default Modal;
