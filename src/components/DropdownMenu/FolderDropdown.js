import React from 'react';
import PropType from 'prop-types';
import classnames from 'classnames';
import './DropdownMenu.scss';

const FolderDropdown = (props) => {
  return (
    <div className={classnames('dropdown folders', {'show': props.visibility})}>
      <div className="dropdown__wrapper">
        {
          props.menus.map((menu) => {
            const style = {backgroundColor: menu.colorCode};
            return (
              <div className="dropdown__menu" onClick={() => {props.onClickMenu(menu);}}>
                <span className="dropdown__menu__text">{menu.name}</span>
                <span className="dropdown__menu__circle" style={style}></span>
              </div>
            );
          })
        }
      </div>
    </div>
  );
};

FolderDropdown.defaultProps = {
  visibility: false,
  menus: [],
  onClickMenu: () => {},
};

FolderDropdown.propTypes = {
  visibility: PropType.bool,
  menus: PropType.array,
  onClickMenu: PropType.func,
};

export default FolderDropdown;
