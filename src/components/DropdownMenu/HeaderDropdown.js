import React from 'react';
import PropType from 'prop-types';
import classnames from 'classnames';
import './DropdownMenu.scss';

const HeaderDropdown = (props) => {
  return (
    <div className={classnames('dropdown', {'show': props.visibility})}>
      {
        props.menus.map((menu) => {
          return (
            <div className={classnames('dropdown__menu', menu.className)} onClick={() => {props.onClickMenu(menu);}}>{menu.name}</div>
          );
        })
      }
    </div>
  );
};

HeaderDropdown.defaultProps = {
  visibility: false,
  menus: [],
  onClickMenu: () => {},
};

HeaderDropdown.propTypes = {
  visibility: PropType.bool,
  menus: PropType.array,
  onClickMenu: PropType.func,
};

export default HeaderDropdown;
