import React, { Component } from 'react';
import {COMMOM} from './../../constants/constants';
import StripeCheckout from 'react-stripe-checkout';
import Button from './../FormComponents/Button/Button';
import CommentIcon from './../../../public/images/icons/chat.svg';
import MultiColorIcon from './../../../public/images/icons/rgb.svg';
import FolderManagement from './../../../public/images/icons/folder-management.svg';
import Integrations from './../../../public/images/icons/integrations.svg';
import ReactGA from 'react-ga';
import './Upgrade.scss';

class Upgrade extends Component {
  constructor(props) {
    super(props);
    this.onToken = this.onToken.bind(this);
    this.onPaypalUpgrade = this.onPaypalUpgrade.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      ReactGA.pageview('/hl/upgrade');
    }, 400);
  }

  onToken(token) {
    this.props.doPayment(token);
  }

  onPaypalUpgrade() {
    ReactGA.event({
      category: 'User',
      action: 'clicked',
      label: 'PAYPAL_UPGRADE_CLICKED',
    });
  }

  render() {
    return (
      <div className="upgrade-page">
        <div className="upgrade-page__title">Highlite Pro Plan - 1 year</div>
        <div className="upgrade-page__key-features">
          <div className="upgrade-page__key-features__sub-title">
            Key Features
          </div>
          <div className="upgrade-page__key-features__feature-list">
            <div className="upgrade-page__key-features__feature-list__row">
              <div className="upgrade-page__key-features__feature-list__each-feature">
                <CommentIcon
                  width={50}
                  height={50}
                />
                Add comments on each highlight
              </div>
              <div className="upgrade-page__key-features__feature-list__each-feature">
                <MultiColorIcon
                  width={50}
                  height={50}
                />
                Add more colors to your highlights
              </div>
            </div>
            <div className="upgrade-page__key-features__feature-list__row">
              <div className="upgrade-page__key-features__feature-list__each-feature">
                <FolderManagement
                  width={50}
                  height={50}
                />
                Smart and Easy Folder management
              </div>
              <div className="upgrade-page__key-features__feature-list__each-feature">
                <Integrations
                  width={50}
                  height={50}
                />
                Sync your readings from pocket and instapaper
              </div>
            </div>
          </div>
          <div className="upgrade-page__key-features__more">... and more</div>
        </div>
        <StripeCheckout
          name="Highlite"
          ComponentClass="upgrade-buttom"
          panelLabel="Upgrade"
          currency="USD"
          stripeKey={COMMOM.STRIPE_TOKEN}
          locale="en"
          email={this.props.user.email}
          shippingAddress={false}
          billingAddress={false}
          zipCode={false}
          alipay
          bitcoin
          allowRememberMe
          token={this.onToken}
          reconfigureOnUpdate={false}
          triggerEvent="onClick"
          >
            <Button
            className="button upgrade" value="Upgrade"/>
        </StripeCheckout>

        <div className="upgrade-page__paypal-account" onClick={this.onPaypalUpgrade}>
          <div className="upgrade-page__paypal-account__text">Or</div>
          <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick"/>
            <input type="hidden" name="hosted_button_id" value="CHGQAEGQQZNNE"/>
            <input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!"/>
            <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1"/>
          </form>
        </div>
      </div>
    );
  }
}

export default Upgrade;
