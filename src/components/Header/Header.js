import React, { Component } from 'react';
import Logo from './../Logo/Logo';
import Button from './../FormComponents/Button/Button';
import Label from './../Labels/Label';
import SVGLoader from './../SVGLoader/';
import AVATAR from './../../../public/images/icons/avatar.svg';
import HeaderDropdown from './../DropdownMenu/HeaderDropdown';
import PropTypes from 'prop-types';
import './Header.scss';

class Header extends Component {
  constructor(props) {
    super(props);
    this.user = {};
  }

  componentDidMount() {
    this.user = this.props.getUser();
  }

  render() {
    return (
      <div className="header">
        <div className="header__logo-wrapper">
          <a href="/hl/highlight">
            <Logo/>
          </a>
        </div>
        <div
          className="header__rest-of-header">
          <div className="header__rest-of-header__user-info"
            onMouseEnter={() => {this.props.toggleDropdown(false);}}
            onMouseLeave={() => {this.props.toggleDropdown(true);}}>
            <div className="header__rest-of-header__user-info__avatar">
              <AVATAR width={35} height={35}/>
            </div>
            <div className="header__rest-of-header__user-info__info-wrapper">
              <div className="header__rest-of-header__user-info__name">{this.user.name}</div>
              <div className="header__rest-of-header__user-info__plan-type">
                {
                  this.user.accountType === 0 &&
                  <Label value="BASIC" className="gray"/>
                }
                {
                  this.user.accountType === 1 &&
                  <Label value="FREE" className="success"/>
                }
              </div>
            </div>
            <HeaderDropdown
              visibility={this.props.showDropdown}
              menus={this.props.menus}
              onClickMenu={(item) => {this.props.onClickMenu(this.context, item, false);}}
            />
          </div>
          {
            this.user.accountType === 0 &&
            <Button
              className="upgrade-now-cta"
              theme="light"
              name="upgrade-now"
              value="Upgrade Now"
              type="button"
              size="medium"
              onClick={() => {this.props.onClickMenu(this.context, this.props.menus[1], true);}}
            />
          }
        </div>
      </div>
    );
  }
}

Header.contextTypes = {
  router: PropTypes.object,
};

export default Header;
