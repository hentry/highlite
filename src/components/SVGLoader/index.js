import React from 'react';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';

const SVGLoader = (props) => {
  return (
    <ReactSVG
      path={props.path}
      className={props.className}
    />
  );
};

SVGLoader.PropTypes = {
  className: PropTypes.string,
  path: PropTypes.string,
};

export default SVGLoader;
