import React, { Component } from 'react';
import NoImageFound from './../../../public/images/no-image-found.png';
import FAV from './../../../public/images/icons/favourite.svg';
import NOTFAV from './../../../public/images/icons/not-favourite.svg';
import DELETE from './../../../public/images/icons/dustbin.svg';
import URL from './../../../public/images/icons/link.svg';
import FACEBOOKICON from './../../../public/images/icons/facebook.svg';
import TWITTERICON from './../../../public/images/icons/twitter.svg';
import SEARCHICON from './../../../public/images/icons/search.svg';
import Input from './../FormComponents/Input/Input';
import ToggleButton from 'react-toggle-button';
import FavouriteToggleIcon from './../ToggleIcons/FavouriteToggleIcon';
import Loader from './../Loader/Loader';
import Overlay from './../Overlay/Overlay';
import FolderIcon from './../../../public/images/icons/folder.svg';
import CloseAllHighlight from './../../../public/images/icons/close-highlight.svg';
import FolderDropdown from './../DropdownMenu/FolderDropdown';
import Tooltip from 'rc-tooltip';
import FacebookProvider, { Share } from 'react-facebook';
import 'rc-tooltip/assets/bootstrap.css';
import ReactGA from 'react-ga';
import { getUser, getToken } from './../../utils/storageUtils';
import PropTypes from 'prop-types';

import './Highlights.scss';

class Highlights extends Component {
  constructor(props) {
    super(props);
    this.onToggle = this.onToggle.bind(this);
    this.getFolderColor = this.getFolderColor.bind(this);
    this.shareOnFB = this.shareOnFB.bind(this);
  }

  componentDidMount() {

    const token = getToken();
    if (token === 'undefined' || !token) {
      this.context.router.history.push('login');
      return;
    }

    if (!this.props.match.folderId) this.props.fetchHighlights(this.props.folders);
    setTimeout(() => {
      ReactGA.pageview('/hl/highlight');
      console.log(getUser().email);
      ReactGA.set({
        userId: getUser().email,
      });
    }, 400);
  }

  componentWillReceiveProps(next) {
    const folderId = next.match.params.folderId;

    if (folderId !== this.props.currentFolderId) {
       this.props.fetchHighlights(this.props.folders, folderId);
    }
  }

  onToggle() {
    this.props.onToggle(this.props.toggleValue);
  }

  getFolderColor(folderColor) {
    return {
      backgroundColor: folderColor,
    };
  }

  shareOnFB(item) {
    ReactGA.event({
      category: 'User',
      action: 'clicked',
      label: 'FB_SHARE_CLICKED',
    });
    FB.ui({
      method: 'share',
      href: item.url,
      appId: '234117307077646',
      hashtag: '#HighLite'
    }, function(response){});
  }

  render() {
    const styles = {
      trackStyle: {
        backgroundColor: 'rgb(207, 221, 245)',
      },
      thumbStyle: {
        height: 25,
        width: 25,
      },
    };
    return (
      <div className="highlights">
        {
          this.props.isLoading &&
          <Overlay className="highlights-page-overlay">
            <Loader />
          </Overlay>
        }
        <div className="highlights__action-bar">
          <div className="highlights__action-bar__search-bar">
            <span className="highlights__action-bar__search-bar__search-icon">
              <SEARCHICON width={15} height={15} />
            </span>
            <Input
              name="highlights search"
              placeholder="Search Highlights"
              onChange={this.props.onSearchHighlights}
              value={this.props.searchInput}
            />
          </div>
          <div className="highlights__action-bar__show-only-favourites">
            <span className="highlights__action-bar__show-only-favourites__text">Show only Favourites</span>
            <div className="highlights__action-bar__show-only-favourites__toggle-wrapper">
              <ToggleButton
                inactiveLabel={'off'}
                activeLabel={'on'}
                colors={{
                  activeThumb: {
                    base: 'rgb(250,250,250)',
                  },
                  inactiveThumb: {
                    base: '#ffffff',
                  },
                  active: {
                    base: '#ECAA27',
                    hover: '#d08d09',
                  },
                  inactive: {
                    base: '#4C4A49',
                    hover: 'rgb(95,96,98)',
                  }
                }}
                thumbAnimateRange={[0, 26]}
                trackStyle={styles.trackStyle}
                thumbStyle={styles.thumbStyle}
                thumbIcon={<FavouriteToggleIcon />}
                value={this.props.toggleValue}
                onToggle={this.onToggle}
              />
            </div>
          </div>
          {
            this.props.currentFolderId &&
            <div className="highlights__action-bar__delete-folder" onClick={() => {this.props.deleteFolder(this.props.currentFolderId, this.context);}}>
              <span>Delete Folder</span>
              <DELETE width={15} height={15} />
            </div>
          }
        </div>
        <div className="highlights__list">
          {
            this.props.list.length === 0 && !this.props.isLoading &&
            <div className="highlights__list__no-content">No Highlights found</div>
          }
          {
            this.props.list && this.props.list.map((item) => {
              return (
                <div className="highlights__list__item">
                  {
                    item.isLoading &&
                    <Overlay className="highlights-page-overlay">
                      <Loader />
                    </Overlay>
                  }
                  {
                    !item.showAllHighlights &&
                    <div>
                      <div className="highlights__list__item__image-holder">
                        {
                          item.image ?
                          <img src={item.image} /> :
                          <img src={NoImageFound}/>
                        }
                      </div>
                      <div className="highlights__list__item__content">
                        <div className="highlights__list__item__url">
                          {item.url}
                        </div>
                        <div className="highlights__list__item__title">{item.title}</div>
                        <div className="highlights__list__item__highlights-length">Highlights: {item.highlights && item.highlights.length}</div>
                        <div className="highlights__list__item__show-all">
                          <a className="highlights__list__item__show-all__btn" onClick={() =>{this.props.showAllHighlights(item);}}>Show Highlights</a>
                        </div>
                        <div className="highlights__list__item__menu-tray">
                          <div className="highlights__list__item__image-holder__favourite" onClick={() => {this.props.toggleFavourite(item);}}>
                            {
                              item.favourite ? 
                              <FAV width={15} height={15}/> :
                              <NOTFAV width={15} height={15}/>
                            }
                          </div>
                          {
                            item.folderColor ?
                            <Tooltip
                              placement="top"
                              overlay={item.folderName}>
                              <div
                                className="highlights__list__item__image-holder__folder-color"
                                onClick={() => {this.props.showFoldersDropDown(item);}}>
                                <span className="highlights__list__item__image-holder__folder-color__circle" style={this.getFolderColor(item.folderColor)}></span>
                              </div>
                            </Tooltip> :
                            <div
                              className="highlights__list__item__image-holder__folder-color"
                              onClick={() => {this.props.showFoldersDropDown(item);}}>
                              <FolderIcon width={15} height={15}/>
                            </div>
                          }
                          <FolderDropdown
                            visibility={item.isShowFoldersDropDown}
                            menus={this.props.folders}
                            onClickMenu={(folder) => {this.props.changeFolder(folder, item);}}
                          />
                          <div className="highlights__list__item__menu-tray__delete" onClick={() => {this.props.deleteHighlight(item);}}>
                            <DELETE width={15} height={15} />
                          </div>
                          <div className="highlights__list__item__menu-tray__delete">
                            <a href={item.url} target="_blank">
                              <URL width={15} height={15} />
                            </a>
                          </div>
                          {
                            /*
                              <div className="highlights__list__item__menu-tray__delete" onClick={() => {this.shareOnFB(item);}}>
                                <FACEBOOKICON width={15} height={15}/>
                              </div>
                              <div className="highlights__list__item__menu-tray__delete">
                                <TWITTERICON width={15} height={15} />
                              </div>
                            */
                          }
                        </div>
                      </div>
                    </div>
                  }
                  {
                    item.showAllHighlights &&
                    <div className="highlights__all-list">
                      <div className="highlights__all-list__title">
                      <div>
                        Highlights
                      </div>
                      <div className="highlights__all-list__title__close-icon" onClick={() => {this.props.showAllHighlights(item);}}>
                        <CloseAllHighlight width={15} height={15} />
                      </div>
                      </div>
                      <div className="highlights__all-list__each-highlight">
                        {
                          item.highlights && item.highlights.length === 0 &&
                          <div className="highlights__all-list__each-highlight__no-content">No Highlights Present in this page</div>
                        }
                        {
                          item.highlights && item.highlights.map((each) => {
                            const style = each.styleOptions && typeof each.styleOptions === "object" ? each.styleOptions : {};
                            return <div className="highlights__all-list__each-highlight__each" style={style}>{each.coreText}</div>;
                          })
                        }
                      </div>
                    </div>
                  }
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

Highlights.contextTypes = {
  router: PropTypes.object,
};

export default Highlights;
