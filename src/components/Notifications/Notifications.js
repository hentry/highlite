import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Notifications.scss';

class Notifications extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showNotification: false,
    };

    this.closeNotification = this.closeNotification.bind(this);
  }
  

  componentWillReceiveProps(next) {
    if (next.show === true) {
      this.setState({
        showNotification: true,
      }, () => {
        this.closeNotification();
      });
    }
  }

  closeNotification() {
    const self = this;
    setTimeout(() => {
      this.setState({
        showNotification: false,
      });
    }, 1500);
  }

  render() {
    return (
      <div className={classnames('notifications', {'show': this.state.showNotification})}>
        {this.props.message}
      </div>
    );
  }
}

Notifications.defaultProps = {
  show: false,
  message: '',
};

Notifications.propTypes = {
  show: PropTypes.bool,
  message: PropTypes.string,
};

export default Notifications;
