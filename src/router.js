import React, { Component, PropTypes } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import LayoutContainer from './containers/Layout/LayoutContainer';
import LoginContainer from './containers/Login/LoginContainer';
import ForgotPassword from './containers/ForgotPassword/ForgotPasswordContainer';
import ResetPassword from './containers/ResetPassword/ResetPasswordContainer';
import SignUp from './containers/SignUp/SignUp';
import Logout from './components/Logout/Logout';
import BaseRoute from './containers/BaseRoute/BaseRouteContainer';
import { getUser } from './actions/LoginActions';
import { showInstallModal } from './actions/CommonActions';
import { getItem } from './utils/storageUtils';
import ReactGA from 'react-ga';

class Routes extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    var user = getItem('user');
    if (!user) return;
    getUser(user.email)();
    ReactGA.initialize('UA-101370041-2');
  }

  render() {
    return (
      <Router basename="/">
        <Switch>
          <Route path="/" component={BaseRoute}/>
        </Switch>
      </Router>
    );
  }
}

export default Routes;
