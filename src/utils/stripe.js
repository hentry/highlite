import {COMMOM} from './../constants/constants';

export const StripeHandler = {
  createInstance: () => {
  	console.log(COMMOM.STRIPE_TOKEN);
    return Stripe(COMMOM.STRIPE_TOKEN);
  },
  createToken: (stripe, options) => {
    stripe.createToken(options).then((result) => {
      console.log(result);
    });
  },
  createElement: (stripe) => {
  	return stripe.elements();
  },
  createCard: (elements) => {
  	return elements.create('card');
  }
};
