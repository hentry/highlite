import { Cookies } from 'react-cookie';

export const getUser = () => {
  const user = new Cookies().get('user', {
    path: '/'
  });
  if (!user || user === 'undefined') return null;
  return user;
};

export const getToken = () => {
  const token = new Cookies().get('token', {
    path: '/'
  });
  if (!token || token === 'undefined') return null;
  return token; 
};

export const setItem = (item, value) => {
  return new Cookies().set(item, value, {
    path: '/'
  });
};

export const removeItem = (item) => {
  return new Cookies().remove(item, {
    path: '/'
  });
};

export const getItem = (item) => {
  const result = new Cookies().get(item, {
    path: '/'
  });
  if (!result || result === 'undefined') return null;
  return result;
};