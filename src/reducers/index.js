import { combineReducers } from 'redux';
import { getUser } from './../utils/storageUtils';
const LoginReducer = (state = { }, action) => {
  switch (action.type) {
    case 'USER_NAME_CHANGED':
    return Object.assign({}, state, {
      email: action.name,
      isAuthFailed: false,
      isUserNotAvailable: false,
    });
    case 'PASSWORD_CHANGED':
    return Object.assign({}, state, {
      password: action.password,
      isAuthFailed: false,
      isUserNotAvailable: false,
    });
    case 'USER_NOT_AVAILABLE':
    return Object.assign({}, state, {
      isAuthFailed: false,
      isUserNotAvailable: true,
      isLoading: false,
    });
    case 'AUTH_FAILED':
    return Object.assign({}, state, {
      isAuthFailed: true,
      isUserNotAvailable: false,
      isLoading: false,
    });

    case 'LOGIN_INITIATED':
    return Object.assign({}, state, {
      isLoading: true,
      isAuthFailed: false,
      isUserNotAvailable: false,
    });

    case 'USER_AUTHENTICATED_SUCCESSFULLY':
    return Object.assign({}, state, {
      isLoading: false,
    });
    default:
    return state;
  }
};


const SignUpReducer = (state = { }, action) => {
  switch (action.type) {
    case 'SIGNUP_USER_NAME_CHANGED':
    return Object.assign({}, state, {
      name: action.name,
    });
    case 'SIGNUP_USER_EMAIL_CHANGED':
    return Object.assign({}, state, {
      email: action.email,
    });
    case 'SIGNUP_PASSWORD_CHANGED':
    return Object.assign({}, state, {
      password: action.password,
    });
    case 'CONFIRM_PASSWORD_CHANGED':
    return Object.assign({}, state, {
      confirmPassword: action.confirmPassword,
    });
    case 'USER_SIGNED_UP':
    return Object.assign({}, state, {
      isSignupSuccess: action.isSignupSuccess,
      name: '',
      email: '',
      password: '',
      confirmPassword: '',
      isLoading: false,
      isPasswordMismatchError: false,
    });
    case 'USER_SIGNUP_STARTED':
    return Object.assign({}, state, {
      isLoading: true,
      isPasswordMismatchError: false,
    });
    case 'PASSWORD_MISMATCH':
    return Object.assign({}, state, {
      isPasswordMismatchError: true,
    });
    default:
    return state;
  }
};

const SidebarReducer = (state = { }, action) => {
  switch (action.type) {
    case 'HOVER_ACTION':
    return Object.assign({}, state, {
      allhighlightsActive: action.isHover,
    });
    case 'HOVER_CREATE_FOLDER':
    return Object.assign({}, state, {
      createFolderActive: action.isHover,
    });
    case 'RECEIVE_FOLDERS':
    return Object.assign({}, state, {
      folders: action.folders,
    });
    case 'ADD_FOLDER':
    return Object.assign({}, state, {
      showInput: true,
    });
    case 'FOLDER_ON_CHANGE':
    return Object.assign({}, state, {
      folderName: action.folderName,
    });
    case 'CLOSE_INPUT':
    return Object.assign({}, state, {
      showInput: false,
    });
    default:
    return state;
  }
};

const defaultState = {
  toggleValue: false,
  list: [],
  allHighlights: [],
  isLoading: true,
  folders: [],
};

const sortList = (array, sortBy) => {
  return array.sort((a, b) => {
    const valueA = a[sortBy].toLowerCase(); // ignore upper and lowercase
    var valueB = b[sortBy].toLowerCase(); // ignore upper and lowercase
    if (valueA < valueB) {
      return -1;
    }
    if (valueA > valueB) {
      return 1;
    }

    // names must be equal
    return 0;
  });
};

const HighlightsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'REQUEST_HIGHLIGHTS':
    return Object.assign({}, state, {
      currentFolderId: action.folderId,
      isLoading: true,
    });
    case 'RECEIVE_HIGHLIGHTS':
    let listOfHighlights = [];
    if (action.highlights.length > 0) {
      listOfHighlights = action.highlights.map((item) => {
        if (!item.folderId) return item;
        if (!state.folders) return item;
        const result = state.folders.filter((folder) => {
          return folder.id === item.folderId;
        });
        if (!result || result.length === 0) return item;
        return Object.assign({}, item, {folderColor: result[0].colorCode, folderName: result[0].name});
      });
    }
    return Object.assign({}, state, {
      list: sortList(listOfHighlights, 'title'),
      allHighlights: listOfHighlights,
      isLoading: false,
    });
    case 'UPDATE_SEARCH_INPUT':
    const result = state.allHighlights.filter(function(o) {
      return Object.keys(o).some(function(k) {
        return o[k] && o[k].toString().toLowerCase().indexOf(action.searchInput) != -1;
      });
    });
    return Object.assign({}, state, {
      searchInput: action.searchInput,
      list: sortList(result, 'title'),
    });
    case 'UPDATE_TOGGLE_VALUE':
    let highlights = [];
    if (action.toggleValue) {
      highlights = state.allHighlights.filter(function(item) {
        return item.favourite === action.toggleValue;
      });
    } else {
      highlights = state.allHighlights;
    }
    return Object.assign({}, state, {
      toggleValue: action.toggleValue,
      list: sortList(highlights, 'title'),
    });
    case 'HIGHLIGHT_LOADING':
    const list = [];
    for (let h = 0, l = state.list.length; h < l; h++) {
      const highlight = state.list[h];
      if (highlight.id === action.highlight.id) {
        highlight.isLoading = true;
      }

      list.push(highlight);
    }

    return Object.assign({}, state, {
      list,
    });
    case 'FOLDERS_RECEIVED':
    
    return Object.assign({}, state, {
      folders: action.folders,
    });
    default:
    case 'SHOW_FOLDERS_DROP_DOWN':
    if (!action.highlight) return state;
    const formattedList = state.list.map((item) => {
      if (item.id === action.highlight.id) {
        item.isShowFoldersDropDown = !item.isShowFoldersDropDown;
        return item;
      }
      return item;
    });
    return Object.assign({}, state, {
      list: sortList(formattedList, 'title'),
    });
    case 'RECEIVE_HIGHLIGHT_ITEMS':
    const listWithHighlights = state.list.map((item) => {
      if (!item.highlights) {
        item.highlights = [];
      }
      for (let p = 0; p < action.highlightItems.length; p++) {
        if (action.highlightItems[p].pageId === item.id) {
          item.highlights.push(action.highlightItems[p]);
        }
      }
      return item;
    });
    return Object.assign({}, state, {
      list: sortList(listWithHighlights, 'title'),
    });
    case 'SHOW_ALL_FOLDERS':
    const changeShowAllHighlightsBoolValue = state.list.map((item) => {
      if (item.id === action.highlight.id) {
        item.showAllHighlights = !item.showAllHighlights;
        return item;
      }
      return item;
    });

    return Object.assign({}, state, {
      list: sortList(changeShowAllHighlightsBoolValue, 'title'),
    });
    case 'SPIN_LOADER':
    return Object.assign({}, state, {
      isLoading: true,
    });
  }
};

const HeaderReducer = (state = {}, action) => {
  switch (action.type) {
    case 'TOGGLE_PROFILE_DROPDOWN':
    return Object.assign({}, state, {
      visibility: action.visibility,
    });
    default:
    return state;
  }
};

const LayoutReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FOLDERS_RECEIVED':
    return Object.assign({}, state, {
      folders: action.folders,
    });
    case 'SHOW_INSTALL_MODAL':
    console.log('show showModal');
    return Object.assign({}, state, {
      showModal: true,
    });
    case 'HIDE_INSTALL_MODAL':
    return Object.assign({}, state, {
      showModal: false,
    });
    default:
    return state;
  }
};


const user = getUser();
let defaultSettingsState = {

};

if (user) {
  defaultSettingsState = {
    name: user.name,
    email: user.email,  
  };
} else {
  defaultSettingsState = {};
}

const SettingsReducer = (state = defaultSettingsState, action) => {
  switch (action.type) {
    case 'SETTINGS_CHANGE':
    return Object.assign({}, state, {
      [action.field]: action.value,
    });
    case 'REQUEST_INFO_SAVE':
    return Object.assign({}, state, {
      isLoading: true,
    });
    case 'INFORMATION_SAVED':
    return Object.assign({}, state, {
      isLoading: false,
    });
    case 'SETTINGS_USER_RECEIVED':
    return Object.assign({},state, {
      name: action.user.name,
      email: action.user.email,
    });
    default:
    return state;
  }
};


const ForgotPasswordReducer = (state = {}, action) => {
  switch (action.type) {
    case 'INPUT_CHANGED_FP':
    return Object.assign({}, state, {
      [action.field]: action.value,
    });
    case 'MAIL_SENT':
    return Object.assign({}, state, {
      isForgotPasswordSuccess: true,
      email: '',
    });
    default:
    return state;
  }
};

const ResetPasswordReducer = (state = {}, action) => {
  switch (action.type) {
    case 'INPUT_CHANGED_RP':
    return Object.assign({}, state, {
      [action.field]: action.value,
    });
    case 'PASSWORD_RESETTED':
    return Object.assign({}, state, {
      isResetted: true,
    });
    default:
    return state;
  }
};

export default combineReducers({
  LoginReducer,
  SignUpReducer,
  SidebarReducer,
  HighlightsReducer,
  HeaderReducer,
  LayoutReducer,
  SettingsReducer,
  ForgotPasswordReducer,
  ResetPasswordReducer,
});
