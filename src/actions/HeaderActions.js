// import fetch from 'isomorphic-fetch';
// import {COMMOM} from './../constants/constants';

export function toggleDropdown(visibility) {
  return {
    type: 'TOGGLE_PROFILE_DROPDOWN',
    visibility,
  };
};
