import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';
import { getToken } from './../utils/storageUtils';

export function userUpgraded(response) {
  return {
    type: 'USER_UPGRADED',
    response,
  };
}

export function doPayment(token, userId) {
  return function(dispatch) {

    const params = {
      source: token.id,
      planId: COMMOM.PAYMENT_ID,
    };

    const options = {
      method: 'PUT',
      headers: {
        'x-access-token': getToken(),
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(params),
    };

    return fetch(COMMOM.API_URL + 'user/upgrade?userId=' + userId, options)
      .then(response => response.json())
      .then((json) => {
        dispatch(userUpgraded(json));
      });
  };
}