import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';
import { getToken, getUser } from './../utils/storageUtils';
import { fetchFoldersFromServer } from './SidebarActions';

export function requestHighlights(userId, folderId) {
  return {
    type: 'REQUEST_HIGHLIGHTS',
    userId,
    folderId,
  };
}

export function makeHighlightLoading(highlight) {
  return {
    type: 'HIGHLIGHT_LOADING',
    highlight,
  };
}

export function receiveHighlights(highlights) {
  return {
    type: 'RECEIVE_HIGHLIGHTS',
    highlights,
  };
}

export function receiveHighlightItems(highlightItems) {
  return {
    type: 'RECEIVE_HIGHLIGHT_ITEMS',
    highlightItems,
  };
}

export function updateSearchInput(searchInput) {
  return {
    type: 'UPDATE_SEARCH_INPUT',
    searchInput,
  };
}

export function updateToggleValue(toggleValue) {
  return {
    type: 'UPDATE_TOGGLE_VALUE',
    toggleValue,
  };
}

export function showFoldersDropDown(highlight) {
  return {
    type: 'SHOW_FOLDERS_DROP_DOWN',
    highlight,
  };
}

export function showAllHighlights(highlight) {
  return {
    type: 'SHOW_ALL_FOLDERS',
    highlight,
  };
}

export function startLoader() {
  return {
    type: 'SPIN_LOADER',
  };
}

export function fetchHightlightsFromServer(userId, folders, folderId) {
  return function(dispatch) {
    dispatch(requestHighlights(userId, folderId));

    const options = {
      method: 'GET',
      headers: {
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
    };

    let url = COMMOM.API_URL + 'highlights?userId=' + userId;
    if (folderId) {
      url = COMMOM.API_URL + 'highlights/byfolder?userId=' + userId + '&folderId=' + folderId;
    }
    return fetch(url, options)
      .then(response => response.json())
      .then((json) => {
        dispatch(receiveHighlights(json));
        fetchHighlightItems(userId)(dispatch);
      });
  };
}

export function updateFavouriteByHighlight(highlight, userId) {
  return function(dispatch) {

    dispatch(makeHighlightLoading(highlight));
    const params = {
      favourite: !highlight.favourite,
      id: highlight.id
    };

    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(params),
    };
    return fetch(COMMOM.API_URL + 'highlights/favourite?userId=' + userId, options)
      .then(response => response.json())
      .then((json) => {
        fetchHightlightsFromServer(userId)(dispatch);
      });
  };
}

export function deleteHighlight(highlight, userId) {
  return function(dispatch) {
    const params = {
      deleted: true,
      id: highlight.id
    };

    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(params),
    };
    return fetch(COMMOM.API_URL + 'highlights/delete?userId=' + userId, options)
    .then(response => response.json())
    .then((json) => {
      fetchHightlightsFromServer(userId)(dispatch);
    });
  };
}

export function changeFolderForHighlight(highlight, folder, userId) {
  return (dispatch) => {
    const data = {
      folderId: folder.id,
      id: highlight.id,
    };

    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(data),
    };
    return fetch(COMMOM.API_URL + 'highlights/folderChange?userId=' + userId, options)
    .then(response => response.json())
    .then((json) => {
      // This has to be changed
      fetchHightlightsFromServer(userId)(dispatch);
    });
  };
}

export function fetchHighlightItems(userId) {
  return function(dispatch) {
    const options = {
      method: 'GET',
      headers: {
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
    };
    return fetch(COMMOM.API_URL + 'highlights/items?userId=' + userId, options)
      .then(response => response.json())
      .then((json) => {
        dispatch(receiveHighlightItems(json));
      });
  };
}

export function deleteFolder(folderId) {
  return (dispatch, context) => {
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
    };
    dispatch(startLoader());
    return fetch(COMMOM.API_URL + 'folders/delete?id=' + folderId, options)
    .then(response => response.json())
    .then((json) => {
      const user = getUser();
      fetchFoldersFromServer(user.id)(dispatch);
      context.router.history.push('/hl/highlight');
    });
  };
}