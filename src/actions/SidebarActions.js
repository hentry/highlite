import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';
import { getToken } from './../utils/storageUtils';
import { foldersReceived } from './LayoutActions';

export function onFolderNameChange(folderName) {
  return {
    type: 'FOLDER_ON_CHANGE',
    folderName,
  };
}

export function addFolder() {
  return {
    type: 'ADD_FOLDER',
  };
}

export function allHighlightsHover(isHover) {
  return {
    type: 'HOVER_ACTION',
    isHover,
  };
}

export function createFolderHover(isHover) {
  return {
    type: 'HOVER_CREATE_FOLDER',
    isHover,
  };
}

export function requestFolders(userId) {
	return {
		type: 'REQUEST_FOLDERS',
		userId,
	};
}

export function receiveFolders(folders) {
	return {
		type: 'RECEIVE_FOLDERS',
		folders,
	};
}

export function closeInput() {
  return {
    type: 'CLOSE_INPUT',
  };
}

export function fetchFoldersFromServer(userId) {
  return function(dispatch) {
    dispatch(requestFolders(userId));

    const options = {
      method: 'GET',
      headers: {
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
    };
    return fetch(COMMOM.API_URL + 'folders?userId=' + userId, options)
      .then(response => response.json())
      .then((json) => {
        dispatch(receiveFolders(json));
        dispatch(foldersReceived(json));
      });
  };
}

export function createFolder(folderName, userId) {
  return function(dispatch) {
    const options = {
      method: 'POST',
      headers: {
        'x-access-token': getToken(),
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify({
        userId: userId,
        name: folderName,
      }),
    };

    return fetch(COMMOM.API_URL + 'folders', options)
      .then(response => response.json())
      .then((json) => {
        dispatch(onFolderNameChange(''));
        dispatch(closeInput());
        fetchFoldersFromServer(userId)(dispatch);
      });
  };
}