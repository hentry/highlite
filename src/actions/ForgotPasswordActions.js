import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';

export function changeListener(value, field) {
  return {
    type: 'INPUT_CHANGED_FP',
    value,
    field
  };
}

export function mailSent() {
  return {
    type: 'MAIL_SENT',
  };
}


export function resetPassword(data) {
  return function(dispatch) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(data),
    };

    return fetch(COMMOM.API_URL + 'user/resetPassword', options)
      .then(response => response.json())
      .then((json) => {
        dispatch(mailSent(json));
      });
  };
}