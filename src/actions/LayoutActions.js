import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';
import { getToken } from './../utils/storageUtils';

export function foldersReceived(folders) {
  return {
    type: 'FOLDERS_RECEIVED',
    folders,
  };
};

export function fetchFoldersFromServer(userId) {
  return function(dispatch) {

    const options = {
      method: 'GET',
      headers: {
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
    };
    return fetch(COMMOM.API_URL + 'folders?userId=' + userId, options)
      .then(response => response.json())
      .then((json) => {
        dispatch(foldersReceived(json));
      });
  };
}

