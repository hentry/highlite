import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';
import ReactGA from 'react-ga';
import { getToken, setItem, getItem } from './../utils/storageUtils';

export function userNameChange(name) {
  return {
    type: 'USER_NAME_CHANGED',
    name,
  };
};

export function passwordChange(password) {
  return {
    type: 'PASSWORD_CHANGED',
    password,
  };
};

export function userReceived(user) {
	return {
		type: 'USER_RECEIVED',
		user
	};
}

export function userNotAvailable() {
  return {
    type: 'USER_NOT_AVAILABLE',
  };
}

export function authFailed() {
  return {
    type: 'AUTH_FAILED',
  };
}

export function loginInitiated() {
  return {
    type: 'LOGIN_INITIATED',
  };
}

export function userAuthenticated() {
  return {
    type: 'USER_AUTHENTICATED_SUCCESSFULLY',
  };
}

export function getUser(userId) {
	return function(dispatch) {
		const options = {
      method: 'GET',
      headers: {
      	'x-access-token': getToken(),
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
    };
    return fetch(COMMOM.API_URL + 'user?userId=' + userId, options)
		  .then(response => response.json())
		  .then((json) => {
		    setItem('user', JSON.stringify(json));
        ReactGA.set({
          userId: getItem('user').email,
        });
		  });
	};
}

export function doLogin(params) {
  return function(dispatch, context) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(params),
    };

    dispatch(loginInitiated());

    return fetch(COMMOM.API_URL + 'users/authenticate', options)
      .then(response => response.json())
      .then((json) => {
        if (json.errorCode) {
          if (json.errorCode === 'user_not_available') {
            dispatch(userNotAvailable());
          } else if (json.errorCode === 'authentication_failed') {
            dispatch(authFailed());
          }
        } else {
          setItem('token', json.token);
          dispatch(userAuthenticated());
          getUser(params.email)(dispatch).then(() => {
            context.router.history.push('hl/highlight');
          });
        }
      });
  };
}