export function showInstallModal() {
  console.log('triggered');
  return {
    type: 'SHOW_INSTALL_MODAL',
  };
};

export function hideInstallModal() {
  return {
    type: 'HIDE_INSTALL_MODAL',
  };
};
