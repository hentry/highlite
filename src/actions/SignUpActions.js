import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';

export function doSignUp(data) {
	return Object.assign({type: 'DO_SIGNUP'}, data);
}

export function userNameChange(name) {
	return {
    type: 'SIGNUP_USER_NAME_CHANGED',
    name,
  };
}

export function userEmailChange(email) {
  return {
    type: 'SIGNUP_USER_EMAIL_CHANGED',
    email,
  }; 
}

export function passwordChange(password) {
  return {
    type: 'SIGNUP_PASSWORD_CHANGED',
    password,
  };
}

export function userSignedUp(user) {
  return {
    type: 'USER_SIGNED_UP',
    user,
    isSignupSuccess: true,
  };
}

export function signupStarted() {
  return {
    type: 'USER_SIGNUP_STARTED',
  };
}


export function confirmPasswordChange(confirmPassword) {
  return {
    type: 'CONFIRM_PASSWORD_CHANGED',
    confirmPassword,
  };
}

export function passwordMismatch() {
  return {
    type: 'PASSWORD_MISMATCH',
  };
}

export function signupUser(params) {
  return function(dispatch) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(params),
    };

    dispatch(signupStarted());

    return fetch(COMMOM.API_URL + 'users', options)
      .then(response => response.json())
      .then((json) => {
        dispatch(userSignedUp(json));
      });
  };
}