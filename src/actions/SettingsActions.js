import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';
import { getUser, setItem, getToken } from './../utils/storageUtils';

export function changeListener(value, field) {
  return {
    type: 'SETTINGS_CHANGE',
    value,
    field,
  };
}

export function requestSave() {
  return {
    type: 'REQUEST_INFO_SAVE',
  };
}

export function infoSaved(data) {
  return {
    type: 'INFORMATION_SAVED',
    data,
  }; 
}

export function userReceived(user) {
  return {
    type: 'SETTINGS_USER_RECEIVED',
    user,
  };
}

export function saveProfileInfo(data) {
  return function(dispatch) {
    dispatch(requestSave());
    const user = getUser();
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(data),
    };

    return fetch(COMMOM.API_URL + 'user?userId=' + user.email, options)
      .then(response => response.json())
      .then((json) => {
        getUserForSettings(user.email)(dispatch);
        dispatch(infoSaved(json));
      });
  };
}

export function changeUserPassword(data) {
  return function(dispatch) {
    dispatch(requestSave());
    const user = getUser();
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(data),
    };

    return fetch(COMMOM.API_URL + 'user/changepassword?userId=' +  user.email, options)
      .then(response => response.json())
      .then((json) => {
        dispatch(infoSaved(json));
      });
  };
}

export function getUserForSettings(userId) {
  return function(dispatch) {
    const options = {
      method: 'GET',
      headers: {
        'x-access-token': getToken(),
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
    };
    return fetch(COMMOM.API_URL + 'user?userId=' + userId, options)
      .then(response => response.json())
      .then((json) => {
        setItem('user', JSON.stringify(json));
        dispatch(userReceived(json));
      });
  };
}
