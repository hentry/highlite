import fetch from 'isomorphic-fetch';
import {COMMOM} from './../constants/constants';

export function changeListener(value, field) {
  return {
    type: 'INPUT_CHANGED_RP',
    value,
    field,
  };
}

export function passwordChanged() {
  return {
    type: 'PASSWORD_RESETTED'
  };
}

export function changePassword(data) {
  return function(dispatch) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify(data),
    };

    return fetch(COMMOM.API_URL + 'user/forgotPassword', options)
      .then(response => response.json())
      .then((json) => {
        dispatch(passwordChanged(json));
      });
  };
}
