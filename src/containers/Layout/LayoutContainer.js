import React from 'react';
import { connect } from 'react-redux';
import {
  fetchFoldersFromServer,
} from './../../actions/LayoutActions';
import {
  hideInstallModal,
} from './../../actions/CommonActions';
import { getUser } from './../../utils/storageUtils';
import Layout from './../../components/Layout/Layout';

const mapStatesToProps = (state) => {
  return {
    folders: state.LayoutReducer.folders,
    user: state.LoginReducer.user,
    showModal: state.LayoutReducer.showModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => {
      const user = getUser();
      fetchFoldersFromServer(user.id)(dispatch);
    },
    installExtension: () => {
      window.open('https://chrome.google.com/webstore/detail/highlite/miheckfeickgiogdbpmfkgnkefcjlokb', '_blank');
      // dispatch(hideInstallModal());
    },
    cancelModal: () => {
      dispatch(hideInstallModal());
    },
  };
};

const LayoutContainer = connect(mapStatesToProps, mapDispatchToProps)(Layout);

export default LayoutContainer;