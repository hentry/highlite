import React from 'react';
import { connect } from 'react-redux';
import SignUp from './../../components/SignUp/SignUp';
import { doSignUp,
userNameChange,
passwordChange,
confirmPasswordChange,
userEmailChange,
signupUser,
passwordMismatch,
} from './../../actions/SignUpActions';

const mapStatesToProps = (state) => {
  return {
    email: state.SignUpReducer.email,
    password: state.SignUpReducer.password,
    confirmPassword: state.SignUpReducer.confirmPassword,
    name: state.SignUpReducer.name,
    isSignupSuccess: state.SignUpReducer.isSignupSuccess,
    isLoading: state.SignUpReducer.isLoading,
    isPasswordMismatchError: state.SignUpReducer.isPasswordMismatchError,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doSignUp: (userName, password) => {
      doSignUp(userName, password);
    },
    onUserNameChange: (e) => {
      dispatch(userNameChange(e.target.value));
    },
    onUserEmailChange: (e) => {
      dispatch(userEmailChange(e.target.value));
    },
    onPasswordChange: (e) => {
      dispatch(passwordChange(e.target.value));
    },
    onConfirmPasswordChange: (e) => {
      dispatch(confirmPasswordChange(e.target.value));
    },
    onClickSignUp: (params) => {
      signupUser(params)(dispatch);
    },
    passwordMismatch: () => {
      dispatch(passwordMismatch());
    },
  };
};

const SignUpContainer = connect(mapStatesToProps, mapDispatchToProps)(SignUp);

export default SignUpContainer;