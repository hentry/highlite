import React from 'react';
import { connect } from 'react-redux';
import Login from './../../components/Login/Login';
import {
  doLogin,
  userNameChange,
  passwordChange,
  getUser,
} from './../../actions/LoginActions';

const mapStatesToProps = (state) => {
  return {
    email: state.LoginReducer.email,
    password: state.LoginReducer.password,
    isUserNotAvailable: state.LoginReducer.isUserNotAvailable,
    isAuthFailed: state.LoginReducer.isAuthFailed,
    isLoading: state.LoginReducer.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doLogin: (email, password, context) => {
      const params = {email, password};
      doLogin(params)(dispatch, context);
    },
    onUserNameChange: (e) => {
      dispatch(userNameChange(e.target.value));
    },
    onPasswordChange: (e) => {
      dispatch(passwordChange(e.target.value));
    },
  };
};

const LoginContainer = connect(mapStatesToProps, mapDispatchToProps)(Login);

export default LoginContainer;