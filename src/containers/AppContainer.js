import React from 'react';
import { connect } from 'react-redux';
import Router from './../router';
import {
  
} from './../../actions/SidebarActions';
import {getUser} from './../../utils/storageUtils';

const mapStatesToProps = (state) => {
  return {
    folders: state.CommonReducer.folders,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleDropdown: (visibility) => {
      dispatch(toggleDropdown(!visibility));
    },
    onClickMenu: (context, menuItem) => {
      if (menuItem.code === 'settings') {
        context.router.history.push('/hl/settings');
      } else if (menuItem.code === 'logout') {
        context.router.history.push('/logout');
      } else if (menuItem.code === 'upgrade') {
        context.router.history.push('/hl/upgrade');
      }
    },
    getUser: () => {
      return getUser();
    }
  };
};

const HeaderContainer = connect(mapStatesToProps, mapDispatchToProps)(Header);

export default HeaderContainer;