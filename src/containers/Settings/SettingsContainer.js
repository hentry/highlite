import React from 'react';
import { connect } from 'react-redux';
import Settings from './../../components/Settings/Settings';
import {
  changeListener,
  saveProfileInfo,
  changeUserPassword,
  getUserForSettings,
} from './../../actions/SettingsActions';

const mapStatesToProps = (state) => {
  return Object.assign({}, state, state.SettingsReducer);
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: (value, field) => {
      dispatch(changeListener(value, field));
    },
    onSaveProfileInfo: (data) => {
      saveProfileInfo(data)(dispatch);
    },
    changePassord: (data) => {
      changeUserPassword(data)(dispatch);
    },
    fetchUser: (user) => {
      getUserForSettings(user.id)(dispatch);
    }
  };
};

const SettingsContainer = connect(mapStatesToProps, mapDispatchToProps)(Settings);

export default SettingsContainer;