import React from 'react';
import { connect } from 'react-redux';
import Sidebar from './../../components/Sidebar/Sidebar';
import {
	addFolder,
	allHighlightsHover,
	createFolderHover,
  fetchFoldersFromServer,
  onFolderNameChange,
  createFolder,
} from './../../actions/SidebarActions';

import { getUser } from './../../utils/storageUtils';
import { withRouter } from 'react-router';
import ReactGA from 'react-ga';

const mapStatesToProps = (state) => {
  return {
    folders: state.SidebarReducer.folders,
    allhighlightsActive: state.SidebarReducer.allhighlightsActive,
    createFolderActive: state.SidebarReducer.createFolderActive,
    showInput: state.SidebarReducer.showInput,
    folderName: state.SidebarReducer.folderName,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addFolder: () => {
      ReactGA.event({
        category: 'User',
        action: 'clicked',
        label: 'ADD_FOLDER',
      });
      dispatch(addFolder());
    },
    allHighlightsMouseEnter: () => {
    	dispatch(allHighlightsHover(true));
    },
    allHighlightsMouseLeave: () => {
    	dispatch(allHighlightsHover(false));
    },
    createFolderMouseEnter: () => {
    	dispatch(createFolderHover(true));
    },
    createFolderMouseLeave: () => {
    	dispatch(createFolderHover(false));
    },
    fetchFolders:() => {
      const user = getUser();
      fetchFoldersFromServer(user.id)(dispatch);
    },
    onFolderNameChange: (event) => {
      dispatch(onFolderNameChange(event.target.value));
    },
    createFolder: (folderName) => {
      if (!folderName) return;
      const user = getUser();
      ReactGA.event({
        category: 'User',
        action: 'clicked',
        label: 'FOLDER_ADDED',
      });
      createFolder(folderName, user.id)(dispatch);
    },
    navigateTo: (path, context) => {
      ReactGA.event({
        category: 'User',
        action: 'navigation',
        label: 'NAVIGATE_TO',
        value: path,
      });
      context.router.history.push(path);
    },
  };
};

const SidebarContainer = withRouter(connect(mapStatesToProps, mapDispatchToProps)(Sidebar));

export default SidebarContainer;