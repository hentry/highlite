import React from 'react';
import { connect } from 'react-redux';
import Upgrade from './../../components/Upgrade/Upgrade';
import {
  doPayment,
} from './../../actions/UpgradeActions';
import { getUser } from './../../utils/storageUtils';

const mapStatesToProps = (state) => {
  return {
    settings: state.SignUpReducer.settings,
    user: getUser(),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doPayment: (token) => {
      console.log(token);
      const user = getUser();
      doPayment(token, user.id)(dispatch);
    },
  };
};

const UpgradeContainer = connect(mapStatesToProps, mapDispatchToProps)(Upgrade);

export default UpgradeContainer;