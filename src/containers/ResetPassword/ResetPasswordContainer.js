import React from 'react';
import { connect } from 'react-redux';
import ResetPassword from './../../components/ResetPassword/ResetPassword';
import {
  changeListener,
  changePassword,
} from './../../actions/ResetPasswordActions';

const mapStatesToProps = (state) => {
  return {
    newpassword: state.ResetPasswordReducer.newpassword,
    isResetted: state.ResetPasswordReducer.isResetted,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: (value, field) => {
      dispatch(changeListener(value, field));
    },
    resetPassword: (data) => {
      changePassword(data)(dispatch);
    }
  };
};

const ForgotPasswordContainer = connect(mapStatesToProps, mapDispatchToProps)(ResetPassword);

export default ForgotPasswordContainer;