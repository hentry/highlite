import React from 'react';
import { connect } from 'react-redux';
import ForgotPassword from './../../components/ForgotPassword/ForgotPassword';
import {
  changeListener,
  resetPassword,
} from './../../actions/ForgotPasswordActions';

const mapStatesToProps = (state) => {
  return {
    email: state.ForgotPasswordReducer.email,
    isForgotPasswordSuccess: state.ForgotPasswordReducer.isForgotPasswordSuccess,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: (value, field) => {
      dispatch(changeListener(value, field));
    },
    resetPassword: (data) => {
      resetPassword(data)(dispatch);
    }
  };
};

const ForgotPasswordContainer = connect(mapStatesToProps, mapDispatchToProps)(ForgotPassword);

export default ForgotPasswordContainer;