import React from 'react';
import { connect } from 'react-redux';
import Header from './../../components/Header/Header';
import {
  toggleDropdown,
} from './../../actions/HeaderActions';
import {getUser} from './../../utils/storageUtils';
import ReactGA from 'react-ga';

const mapStatesToProps = (state) => {
  return {
    menus: [
      {
        name: 'Profile',
        code: 'settings',
        className: 'dropdown-settings',
      },
      {
        name: 'Log Out',
        code: 'logout',
        className: 'dropdown-logout',
      },
    ],
    showDropdown: state.HeaderReducer.visibility,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleDropdown: (visibility) => {
      dispatch(toggleDropdown(!visibility));
    },
    onClickMenu: (context, menuItem, isHeader) => {
      if (menuItem.code === 'settings') {
        ReactGA.event({
          category: 'User',
          action: 'clicked',
          label: 'SETTINGS_CLICKED',
        });
        context.router.history.push('/hl/settings');
      } else if (menuItem.code === 'logout') {
        ReactGA.event({
          category: 'User',
          action: 'clicked',
          label: 'LOGOUT_CLICKED',
        });
        context.router.history.push('/logout');
      } else if (menuItem.code === 'upgrade') {
        ReactGA.event({
          category: 'User',
          action: 'clicked',
          label: isHeader ? 'UPGRADE_HEADER_CLICKED' : 'UPGRADE_DROPDOWN_CLICKED',
        });
        context.router.history.push('/hl/upgrade');
      }
    },
    getUser: () => {
      return getUser();
    }
  };
};

const HeaderContainer = connect(mapStatesToProps, mapDispatchToProps)(Header);

export default HeaderContainer;