import React from 'react';
import { connect } from 'react-redux';
import Highlights from './../../components/Highlights/Highlights';
import { 
  fetchHightlightsFromServer,
  updateSearchInput,
  updateToggleValue,
  filterHighlights,
  updateFavouriteByHighlight,
  deleteHighlight,
  showFoldersDropDown,
  changeFolderForHighlight,
  showAllHighlights,
  deleteFolder,
} from './../../actions/HighlightsActions';
import { getUser } from './../../utils/storageUtils';
import ReactGA from 'react-ga';

const mapStatesToProps = (state) => {
  return {
    list: state.HighlightsReducer.list,
    searchInput: state.HighlightsReducer.searchInput,
    toggleValue: state.HighlightsReducer.toggleValue,
    isLoading: state.HighlightsReducer.isLoading,
    folders: state.LayoutReducer.folders,
    currentFolderId: state.HighlightsReducer.currentFolderId,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchHighlights: (folders, folderId) => {
      const user = getUser();
      fetchHightlightsFromServer(user.id, folders, folderId)(dispatch);
    },
    onSearchHighlights: (event) => {
      dispatch(updateSearchInput(event.target.value));
    },
    onToggle: (toggleValue) => {
      ReactGA.event({
        category: 'User',
        action: 'clicked',
        label: 'FILTER_FAV_TOGGLED',
      });
      dispatch(updateToggleValue(!toggleValue));
    },
    toggleFavourite: (highlight) => {
      const user = getUser();
      ReactGA.event({
        category: 'User',
        action: 'clicked',
        label: 'FAVOURITE_TOGGLED',
      });
      updateFavouriteByHighlight(highlight, user.id)(dispatch);
    },
    deleteHighlight: (highlight) => {
      const value = confirm('Are you sure to delete this highlight');
      if (value) {
        ReactGA.event({
          category: 'User',
          action: 'clicked',
          label: 'HIGHLIGHT_DELETED',
        });
        const user = getUser();
        deleteHighlight(highlight, user.id)(dispatch);
      }
    },
    showFoldersDropDown: (item) => {
      dispatch(showFoldersDropDown(item));
    },
    changeFolder: (folder, highlight) => {
      const user = getUser();
      ReactGA.event({
        category: 'User',
        action: 'clicked',
        label: 'FOLDER_CHANGED',
      });
      changeFolderForHighlight(highlight, folder, user.id)(dispatch);
    },
    showAllHighlights: (item) => {
      dispatch(showAllHighlights(item));
    },
    deleteFolder: (folderId, context) => {
      const value = confirm('Are you sure to delete this Folder');
      if (value) {
        ReactGA.event({
          category: 'User',
          action: 'clicked',
          label: 'DELETE_FOLDER',
        });
        deleteFolder(folderId)(dispatch, context);
      }
    },
  };
};

const HighlightsContainer = connect(mapStatesToProps, mapDispatchToProps)(Highlights);

export default HighlightsContainer;