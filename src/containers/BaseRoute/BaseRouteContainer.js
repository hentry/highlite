import React from 'react';
import { connect } from 'react-redux';
import BaseRoute from './../../components/BaseRoute/BaseRoute';
import {
  showInstallModal,
} from './../../actions/CommonActions';
import { getUser } from './../../utils/storageUtils';

const mapStatesToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    showInstallModal: () => {
      dispatch(showInstallModal());
    },
  };
};

const BaseRouteContainer = connect(mapStatesToProps, mapDispatchToProps)(BaseRoute);

export default BaseRouteContainer;